import React from "react"
import styled from "styled-components"
import { Place } from "@material-ui/icons"

import { Section, Container } from "../global"

const Opinions = ({ language }) => (
  <Section id={language === "fr" ? "les avis" : "opinions"}>
    <StyledContainer>
      <SectionTitle>
        {language === "fr"
          ? "Ils ont pu l'essayer"
          : "They could have tried it"}
      </SectionTitle>
    </StyledContainer>
    <OpinionsGrid>
      <OpinionsItem>
        <OpinionsMsg>
          {language === "fr"
            ? "« En tant que développeuse freelance, Haibu me permet de rencontrer d'autres freelances et d'échanger avec eux dans les espaces de coworking en toute convivialité. »"
            : "« As a freelance developer, Haibu allows me to meet other freelancers and exchange with them in coworking spaces in a friendly atmosphere. »"}
        </OpinionsMsg>
        <OpinionsText>
          <Place />
          Paris, Stéphanie J.
        </OpinionsText>
      </OpinionsItem>
      <OpinionsItem>
        <OpinionsMsg>
          {language === "fr"
            ? "« Je suis nomade digital, je me sers de Haibu pour rencontrer et échanger avec d'autres nomades français ainsi que pour travailler dans des lieux insolites. »"
            : "« I am a digital nomad, I use Haibu to meet and exchange with other French nomads as well as to work in unusual places. »"}
        </OpinionsMsg>
        <OpinionsText>
          <Place />
          Bali, Matthieu L.
        </OpinionsText>
      </OpinionsItem>
      <OpinionsItem>
        <OpinionsMsg>
          {language === "fr"
            ? "« Je suis devenue freelance pour fuir la routine du travail et Haibu me permet de changer régulièrement d'environnement. Je trouve cela très inspirant. »"
            : "« I became a freelancer to get away from the routine of work and Haibu allows me to regularly change my environment. I find it very inspiring. »"}
        </OpinionsMsg>
        <OpinionsText>
          <Place />
          Lyon, Camille P.
        </OpinionsText>
      </OpinionsItem>
      <OpinionsItem>
        <OpinionsMsg>
          {language === "fr"
            ? "« En général les coworking proposent des forfaits au mois, on se sent obligé d'y aller tous les jours. Avec Haibu c'est à la journée et cela m'a tout de suite plus. Et les lieux sont aussi différents les uns des autres. »"
            : "« Usually coworking offers monthly packages, you feel obliged to go every day. With Haibu it's by the day and I immediately felt more. And the places are also different from each other. »"}
        </OpinionsMsg>
        <OpinionsText>
          <Place />
          Marseille, Pierre V.
        </OpinionsText>
      </OpinionsItem>
    </OpinionsGrid>
  </Section>
)

export default Opinions

const StyledContainer = styled(Container)``

const SectionTitle = styled.h3`
  color: ${props => props.theme.color.primary};
  display: flex;
  justify-content: center;
  margin: 0 auto 40px;
  text-align: center;
`

const OpinionsGrid = styled.div`
  max-width: 670px;
  display: grid;
  grid-template-columns: 1fr;
  margin: 0px auto;
  grid-column-gap: 40px;
  grid-row-gap: 35px;
  @media (max-width: ${props => props.theme.screen.sm}) {
    grid-template-columns: 1fr;
    padding: 0 64px;
  }
`

const OpinionsItem = styled.div`
  display: flex;
  flex-direction: column;
`

const OpinionsMsg = styled.h4`
  color: ${props => props.theme.color.primary};
  letter-spacing: 0px;
  line-height: 30px;
  margin-bottom: 10px;
  text-align: left;
`

const OpinionsText = styled.p`
  text-align: right;
`
