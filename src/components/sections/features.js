import React from "react"
import styled from "styled-components"
import { Group, Language, Payment, GroupWork } from "@material-ui/icons"

import { Section, Container } from "../global"

const Features = ({ language }) => (
  <Section id={language === "fr" ? "les fonctionnalités" : "features"}>
    <StyledContainer>
      <SectionTitle>
        {language === "fr"
          ? "Des espaces inoccupé en journée qui n'attendent que vous."
          : "Daytime unoccupied spaces just waiting for you."}
      </SectionTitle>
      <FeaturesGrid>
        <FeatureItem>
          <Group fontSize="large" style={{ fontSize: "4.1875rem" }} />
          <FeatureTitle>
            {language === "fr" ? "Communauté" : "Community"}
          </FeatureTitle>
          <FeatureText>
            {language === "fr"
              ? "Créez votre réseau professionnel grace à la diversité de nos profils."
              : "Create your professional network with the variety of our profiles."}
          </FeatureText>
        </FeatureItem>
        <FeatureItem>
          <Language fontSize="large" style={{ fontSize: "4.1875rem" }} />
          <FeatureTitle>International</FeatureTitle>
          <FeatureText>
            {language === "fr"
              ? "Découvrez des espaces de coworking à travers le monde."
              : "Discover coworking areas around the world."}
          </FeatureText>
        </FeatureItem>
        <FeatureItem>
          <GroupWork fontSize="large" style={{ fontSize: "4.1875rem" }} />
          <FeatureTitle>Coworking</FeatureTitle>
          <FeatureText>
            {language === "fr"
              ? "Restaurant, bar, café, lieux insolites... Tous sont prêts à vous accueillir."
              : "Restaurants, bars, unusual places... All are ready for you welcome."}
          </FeatureText>
        </FeatureItem>
        <FeatureItem>
          <Payment fontSize="large" style={{ fontSize: "4.1875rem" }} />
          <FeatureTitle>
            {language === "fr" ? "Avantages" : "Benefits"}
          </FeatureTitle>
          <FeatureText>
            {language === "fr"
              ? "Bénéficiez de tarifs très avantageux dans vos espaces."
              : "Benefit from very advantageous rates in your spaces."}
          </FeatureText>
        </FeatureItem>
      </FeaturesGrid>
    </StyledContainer>
  </Section>
)

export default Features

const StyledContainer = styled(Container)``

const SectionTitle = styled.h3`
  color: ${props => props.theme.color.primary};
  display: flex;
  justify-content: center;
  margin: 0 auto 40px;
  text-align: center;
`

const FeaturesGrid = styled.div`
  max-width: 670px;
  display: grid;
  grid-template-columns: 1fr 1fr;
  margin: 0px auto;
  grid-column-gap: 40px;
  grid-row-gap: 35px;
  @media (max-width: ${props => props.theme.screen.sm}) {
    grid-template-columns: 1fr;
    padding: 0 64px;
  }
`

const FeatureItem = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`

const FeatureTitle = styled.h4`
  color: ${props => props.theme.color.primary};
  letter-spacing: 0px;
  line-height: 30px;
  margin-bottom: 10px;
`

const FeatureText = styled.p`
  text-align: center;
`
