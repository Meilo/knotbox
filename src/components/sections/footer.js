import React from "react"
import styled from "styled-components"

import { Container } from "../global"
import insta from "../../images/product/instagram.svg"

const Footer = ({ language }) => (
  <FooterWrapper id="footer">
    {/* <FooterColumnContainer>
      <FooterColumn>
        <span>Features</span>
        <ul>
          <li>Automation</li>
          <li>Rewards</li>
        </ul>
      </FooterColumn>
      <FooterColumn>
        <span>Resources</span>
        <ul>
          <li>Compare</li>
          <li>Blog</li>
        </ul>
      </FooterColumn>
      <FooterColumn>
        <span>Company</span>
        <ul>
          <li>About Us</li>
          <li>Careers</li>
        </ul>
      </FooterColumn>
      <FooterColumn>
        <span>Social</span>
        <ul>
          <li>LinkedIn</li>
          <li>Instagram</li>
        </ul>
      </FooterColumn>
    </FooterColumnContainer> */}
    <BrandContainer>
      <Logo>
        {language === "fr" ? "Suivez-nous sur :" : "Follow us on:"}&nbsp;
        <a
          href="https://www.instagram.com/haibu.app/?hl=fr"
          rel="noreferrer"
          target="_blank"
        >
          <img src={insta} alt="instagram" width="40" />
        </a>
      </Logo>
    </BrandContainer>
    <BrandContainer>
      <Logo>
        © 2020 Haibu.{" "}
        {language === "fr" ? "Tous droits réservés." : "All rights reserved."}
      </Logo>
    </BrandContainer>
    <BrandContainer>
      {language === "fr" ? (
        <a
          style={{ fontSize: 13, color: "#000", textDecoration: "none" }}
          href="/politique-de-confidentialite"
        >
          Politique de confidentialité
        </a>
      ) : (
        <a
          style={{ fontSize: 13, color: "#000", textDecoration: "none" }}
          href="/politique-de-confidentialite"
        >
          Privacy Policy
        </a>
      )}
    </BrandContainer>
  </FooterWrapper>
)

const FooterWrapper = styled.footer`
  background-color: white;
  margin: 80px 0 0;
  padding: 0 0 80px;
`

const Logo = styled.div`
  font-family: ${props => props.theme.font.extrabold};
  ${props => props.theme.font_size.regular};
  color: ${props => props.theme.color.black.regular};
  text-decoration: none;
  letter-spacing: 1px;
  margin: 0;
  display: inline-flex;
  justify-content: center;
  align-items: center;
  position: relative;
  z-index: 9;
  text-decoration: none;
  outline: 0px;
`

const BrandContainer = styled(Container)`
  position: relative;
  padding-top: 48px;
  display: flex;
  align-items: flex-end;

  @media (max-width: ${props => props.theme.screen.sm}) {
  }
`
// const FooterColumnContainer = styled(Container)`
//   display: grid;
//   grid-template-columns: repeat(4, 1fr);
//   grid-column-gap: 32px;
//   justify-content: start;
//   @media (max-width: ${props => props.theme.screen.sm}) {
//     grid-template-columns: 1fr 1fr;
//     grid-gap: 32px;
//   }
// `
// const FooterColumn = styled.div`
//   span {
//     font-size: 16px;
//     font-family: ${props => props.theme.font.bold};
//     color: ${props => props.theme.color.primary};
//   }
//   ul {
//     list-style: none;
//     margin: 16px 0;
//     padding: 0;
//     color: ${props => props.theme.color.black.regular};
//     li {
//       margin-bottom: 12px;
//       font-family: ${props => props.theme.font.normal};
//       font-size: 15px;
//     }
//   }
// `

export default Footer
