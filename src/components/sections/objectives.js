import React from "react"
import styled from "styled-components"
import { FlightTakeoff } from "@material-ui/icons"

import { Section, Container } from "../global"

const Objectives = ({ language }) => (
  <Section id={language === "fr" ? "notre objectif" : "objectives"}>
    <StyledContainer>
      <SectionTitle>
        {language === "fr"
          ? "Démocratisons le concepte le plus rapidement possible"
          : "Democratized as quickly as possible"}
      </SectionTitle>
    </StyledContainer>
    <ObjectivesGrid>
      <ObjectivesItem>
        <FlightTakeoff style={{ fontSize: "4.1875rem" }} />
        <ObjectivesMsg>
          {language === "fr"
            ? "Notre objectif, atteindre les 1000 fondateurs Haibu."
            : "Our goal, to reach the 1000 Haibu founders."}
        </ObjectivesMsg>
        <ObjectivesText>
          {language === "fr"
            ? "Nous y sommes presque, il ne manque plus que vous."
            : "We're almost there, all we need now is you."}
        </ObjectivesText>
      </ObjectivesItem>
    </ObjectivesGrid>
  </Section>
)

export default Objectives

const StyledContainer = styled(Container)``

const SectionTitle = styled.h3`
  color: ${props => props.theme.color.primary};
  display: flex;
  justify-content: center;
  margin: 0 auto 40px;
  text-align: center;
`

const ObjectivesGrid = styled.div`
  max-width: 670px;
  display: grid;
  grid-template-columns: 1fr;
  margin: 0px auto;
  grid-column-gap: 40px;
  grid-row-gap: 35px;
  @media (max-width: ${props => props.theme.screen.sm}) {
    grid-template-columns: 1fr;
    padding: 0 64px;
  }
`

const ObjectivesItem = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`

const ObjectivesMsg = styled.h4`
  color: ${props => props.theme.color.primary};
  letter-spacing: 0px;
  line-height: 30px;
  margin-bottom: 10px;
  text-align: left;
`

const ObjectivesText = styled.p`
  text-align: center;
`
