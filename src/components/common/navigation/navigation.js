import React from "react"
import AnchorLink from "react-anchor-link-smooth-scroll"
import Scrollspy from "react-scrollspy"
import { Menu, X } from "react-feather"
import hive from "../../../images/product/hive.svg"

import { Container } from "../../global"
import {
  Nav,
  NavItem,
  Brand,
  StyledContainer,
  NavListWrapper,
  MobileMenu,
  Mobile,
  ActionsContainer,
} from "./style"

const NAV_ITEMS = ["Les fonctionnalités", "Les avis", "Notre objectif"]
const NAV_ITEMS_EN = ["Features", "Opinions", "Objectives"]

export default props => {
  const [mobileMenuOpen, setMobileMenuOpen] = React.useState(false)
  const [hasScrolled, setHasScrolled] = React.useState(false)

  React.useEffect(() => {
    window.addEventListener("scroll", handleScroll)
  })

  const handleScroll = event => {
    const scrollTop = window.pageYOffset

    if (scrollTop > 32) {
      setHasScrolled(true)
    } else {
      setHasScrolled(false)
    }
  }

  const toggleMobileMenu = () => {
    setMobileMenuOpen(!mobileMenuOpen)
  }

  const closeMobileMenu = () => {
    if (mobileMenuOpen) {
      setMobileMenuOpen(false)
    }
  }

  const getNavAnchorLink = item => (
    <AnchorLink href={`#${item.toLowerCase()}`} onClick={closeMobileMenu}>
      {item}
    </AnchorLink>
  )

  const getNavList = ({ mobile = false }) => {
    const items = props.language === "fr" ? NAV_ITEMS : NAV_ITEMS_EN
    return (
      <NavListWrapper mobile={mobile}>
        <Scrollspy
          items={items.map(item => item.toLowerCase())}
          currentClassName="active"
          mobile={mobile}
          offset={-64}
        >
          {items.map(navItem => (
            <NavItem key={navItem}>{getNavAnchorLink(navItem)}</NavItem>
          ))}
        </Scrollspy>
      </NavListWrapper>
    )
  }

  return (
    <Nav {...props} scrolled={hasScrolled} mobile={mobileMenuOpen}>
      <StyledContainer>
        <Brand>
          <Scrollspy offset={-64} item={["top"]} currentClassName="active">
            <AnchorLink href="#top" onClick={closeMobileMenu}>
              <img src={hive} alt="hive" />
              Haibu
            </AnchorLink>
          </Scrollspy>
        </Brand>
        <Mobile hide>{getNavList({})}</Mobile>
        {props.language === "fr" ? (
          <button
            onClick={() => props.setLanguage("en")}
            style={{ fontWeight: "bold", cursor: "pointer" }}
          >
            EN 🇬🇧
          </button>
        ) : (
          <button
            onClick={() => props.setLanguage("fr")}
            style={{ fontWeight: "bold", cursor: "pointer" }}
          >
            FR 🇫🇷
          </button>
        )}
        <Mobile>
          <button
            onClick={toggleMobileMenu}
            style={{ color: "black", background: "none" }}
          >
            {mobileMenuOpen ? (
              <X size={24} alt="close menu" />
            ) : (
              <Menu size={24} alt="open menu" />
            )}
          </button>
        </Mobile>
        {hasScrolled ? (
          <ActionsContainer>
            <AnchorLink href="#top" style={{ textDecoration: "none" }}>
              <button>
                {props.language === "fr"
                  ? "Compte premium gratuit"
                  : "Free premium account"}
              </button>
            </AnchorLink>
          </ActionsContainer>
        ) : (
          <ActionsContainer />
        )}
      </StyledContainer>
      <Mobile>
        {mobileMenuOpen && (
          <MobileMenu>
            <Container>{getNavList({ mobile: true })}</Container>
          </MobileMenu>
        )}
      </Mobile>
    </Nav>
  )
}

// export default class Navigation extends Component {
//   state = {
//     mobileMenuOpen: false,
//     hasScrolled: false,
//   }

//   componentDidMount() {
//     window.addEventListener("scroll", this.handleScroll)
//   }

//   handleScroll = event => {
//     const scrollTop = window.pageYOffset

//     if (scrollTop > 32) {
//       this.setState({ hasScrolled: true })
//     } else {
//       this.setState({ hasScrolled: false })
//     }
//   }

//   toggleMobileMenu = () => {
//     this.setState(prevState => ({ mobileMenuOpen: !prevState.mobileMenuOpen }))
//   }

//   closeMobileMenu = () => {
//     if (this.state.mobileMenuOpen) {
//       this.setState({ mobileMenuOpen: false })
//     }
//   }

//   getNavAnchorLink = item => (
//     <AnchorLink href={`#${item.toLowerCase()}`} onClick={this.closeMobileMenu}>
//       {item}
//     </AnchorLink>
//   )

//   getNavList = ({ mobile = false }) => (
//     <NavListWrapper mobile={mobile}>
//       <Scrollspy
//         items={NAV_ITEMS.map(item => item.toLowerCase())}
//         currentClassName="active"
//         mobile={mobile}
//         offset={-64}
//       >
//         {NAV_ITEMS.map(navItem => (
//           <NavItem key={navItem}>{this.getNavAnchorLink(navItem)}</NavItem>
//         ))}
//       </Scrollspy>
//     </NavListWrapper>
//   )

//   render() {
//     const { mobileMenuOpen } = this.state

//     return (
//       <Nav {...this.props} scrolled={this.state.hasScrolled}>
//         <StyledContainer>
//           <Brand>
//             <Scrollspy offset={-64} item={["top"]} currentClassName="active">
//               <AnchorLink href="#top" onClick={this.closeMobileMenu}>
//                 <img src="hive.svg" />
//                 Haibu
//               </AnchorLink>
//             </Scrollspy>
//           </Brand>
//           <Mobile>
//             <button
//               onClick={this.toggleMobileMenu}
//               style={{ color: "black", background: "none" }}
//             >
//               {this.state.mobileMenuOpen ? (
//                 <X size={24} alt="close menu" />
//               ) : (
//                 <Menu size={24} alt="open menu" />
//               )}
//             </button>
//           </Mobile>

//           <Mobile hide>{this.getNavList({})}</Mobile>
//           {this.state.hasScrolled ? (
//             <ActionsContainer>
//               <AnchorLink href="#top" style={{ textDecoration: "none" }}>
//                 <button>Compte premium gratuit</button>
//               </AnchorLink>
//             </ActionsContainer>
//           ) : (
//             <ActionsContainer />
//           )}
//         </StyledContainer>
//         <Mobile>
//           {mobileMenuOpen && (
//             <MobileMenu>
//               <Container>{this.getNavList({ mobile: true })}</Container>
//             </MobileMenu>
//           )}
//         </Mobile>
//       </Nav>
//     )
//   }
// }
