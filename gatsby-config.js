module.exports = {
  siteMetadata: {
    title: `Haibu`,
    description: `Haibu, du coworking dans vos établissements préférés`,
    author: `Ludovic Elice`,
    siteUrl: `https://wwww.haibu.app`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-styled-components`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `product`,
        path: `${__dirname}/src/images/product`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-plugin-robots-txt`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#ffb101`,
        theme_color: `#ffb101`,
        display: `minimal-ui`,
        icon: `src/images/hiveColor.svg`,
      },
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: "UA-176111986-1",
        // this option places the tracking script into the head of the DOM
        head: true,
        // other options
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
